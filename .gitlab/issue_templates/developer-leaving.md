## Welcome to YOUR leaving issue!

Sad to see you leave Beautiful Canoe :canoe: :sob:

If you have any questions along the way, please feel free to ask on [Slack](https://beautifulcanoe.slack.com/), our chat communication tool, in the `#general` channel.

If you have any suggestions about leaving, please create a merge request in [this repository](https://gitlab.com/beautifulcanoe/peopleops/orientation-templates) and assign it to the CTO to make this documentation better for the next leaving team member.

Just like the orientation this issue is divided into various tasks, you should focus on the **Developer** tasks.
If you can't move forward with your own tasks because someone else hasn't checked tasks that they are responsible for, please remind them by @ mentioning them in the comments for this issue.

---

## Contents

[[_TOC_]]

---

## Before leaving Beautiful Canoe

### CTO tasks

1. [ ] CTO: Create a confidential leaving issue in the [orientation](https://gitlab.com/beautifulcanoe/peopleops/orientation) project.
1. [ ] CTO: Remove developer from `developers` group on GitLab.
1. [ ] CTO: Remove developer from their project repositories group on GitLab.
1. [ ] CTO: Run (or arrange) a final week retrospective, on **Friday**.
1. [ ] CTO: Raise an issue and MR in the [company website repository](https://gitlab.com/beautifulcanoe/identity/beautifulcanoe.com) to update the company statistics.

### Developer tasks

1. [ ] Developer: Finish all unfinished work.
1. [ ] Developer: Update any documentation that needs updating.
1. [ ] Developer: Raise MR to remove self from the [company website](https://beautifulcanoe.com/).
