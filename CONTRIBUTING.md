# Contributing to the Bob the Bot

[[_TOC_]]

---

## Getting started

### Clone this repository

From the command line, run:

```sh
git clone git@gitlab.com:beautifulcanoe/peopleops/peopleops-templates.git
cd peopleops-templates
```

### Ensuring that your Markdown syntax is valid

To make sure that the Markdown documentation in this repository is valid, please use [the mdl Markdown lint](https://github.com/markdownlint/markdownlint).

To install the lint on Debian-like machines, use the [Rubygems](https://rubygems.org/) package manager:

```sh
sudo apt-get install gem
sudo gem install mdl
```

Because we keep each sentence on a separate line, you will want to suppress spurious `MD013 Line length` reports by configuring `mdl`.
The file [.mdl.rb](/.mdl.rb) contains styles that deal with `MD013` and other tweaks we want to make to the lint.
To use the style configuration, pass it as a parameter to `mdl` on the command line:

```sh
mdl -s .mdl.rb DOCUMENT.md
```

If you want to run `mdl` from your IDE or editor, you will either need to configure it, or find a plugin, such as [this one for Sublime Text](https://github.com/SublimeLinter/SublimeLinter-mdl).

### Setting up Git hooks

Git is able to run scripts that will check your code, and prevent you from pushing or committing code that does not pass a lint, test suite or similar.
Git uses *hooks* to do this, which are stored in the directory [`.git/hooks`](/.git/hooks).
This repository contains a hook which will run [mdl](https://github.com/markdownlint/markdownlint) and prevent you from committing code which does not pass the lint.
To set this up, run the [`bin/create-hook-symlinks`] script:

```sh
./bin/create-hook-symlinks
```
